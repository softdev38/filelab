/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natthawadee
 */
public class ReadPlayer {
    public static void main(String[] args) {
        player o = null;
        player x = null;
        
        FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
           o = (player) ois.readObject();
           x = (player) ois.readObject();
          
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            o = new player('o');
            x = new player('x');
            
        } catch (IOException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(fis!=null){
                fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }  
        System.out.println(o);
            System.out.println(x);
    }
}
